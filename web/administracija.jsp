<%-- 
    Document   : administracija
    Created on : Jul 16, 2017, 5:09:32 PM
    Author     : FCCZ
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" rel="stylesheet" type="text/css">
        <title>Mobil Shop</title>
    </head>
    <body>
        <div id="osnovni"> <!-- pocetak osnovnog diva -->
            <div id="menu"> <!-- pocetak diva menu -->
                <a href="index.jsp"><img class="menuImg" src="slike/slikeStranice/logo.jpg" width="150px"></a>
                <a href="administracija.jsp"><p class="menuOpcije">Login</p></a>
                <a href="kontakt.jsp"><p class="menuOpcije">Kontakt</p></a>
                <a href="onama.jsp"><p class="menuOpcije">O nama</p></a>
                <a href="index.jsp"><p class="menuOpcije">Početna</p></a>
                
            </div> <!-- kraj diva menu -->
            <c:if test="${sessionScope.ulogovan!=true}">
                <c:if test="${poruka!=null}" >
                    <div><p class="greska">${poruka}</p><br><br></div>
                </c:if>
            <div class="logovanje">
                
                <form method="post" action="login">
                    <div class="loginPolja"><input placeholder="Username" type="text" name="username"></div>
                    <div class="loginPolja"><input placeholder="Password" type="password" name="password"></div>
                    <div class="login">
                    <input type="submit" value="Login">
                    </div>
                </form>
            </div>
             </c:if>
            
           
        <div id="sredina">
            <c:if test="${sessionScope.ulogovan==true}">
            <c:if test="${poruka!=null}" >
                    <div><p class="greska">${poruka}</p><br><br></div>
            </c:if>
            <form method="post" action="dodajMobilni" enctype="multipart/form-data">
                <div class="zaglavljeAdministracija">
                <select name="proizvodjac">
                            <option value="0">Svi proizvođači</option>
                            <option value="samsung">Samsung</option>
                            <option value="nokia">Nokia</option>
                            <option value="sony">Sony</option>
                            <option value="iphone">Apple</option>
                            <option value="lg">LG</option>
                            <option value="htc">HTC</option>
                            <option value="microsoft">Microsoft</option>
                            <option value="lenovo">Lenovo</option>
                            <option value="huawei">Huawei</option>
                </select>
                </div>
                <div>
                <span class="span"><input type="text" name="cena" placeholder="Cena"></span>
                <span class="span"><input type="text" name="naziv" placeholder="Naziv"></span>
                <span class="span"><input type="text" name="dvaG" placeholder="Dva G"></span>
                <span class="span"><input type="text" name="triG" placeholder="Tri G"></span>
                <span class="span"><input type="text" name="cetiriG" placeholder="Cetiri G"></span>
                <span class="span"><input type="text" name="proizvedeno" placeholder="Proizvedeno"></span>
                <span class="span"><input type="text" name="simKartica" placeholder="Sim kartica"></span>
                <span class="span"><input type="text" name="dimenzije" placeholder="Dimenzije"></span>
                <span class="span"><input type="text" name="masa" placeholder="Masa"></span>
                <span class="span"><input type="text" name="tastatura" placeholder="Tastatura"></span>
                <span class="span"><input type="text" name="tipBaterije" placeholder="Tip baterije"></span>
                <span class="span"><input type="text" name="tipEkrana" placeholder="Tip ekrana"></span>
                <span class="span"><input type="text" name="velicinaEkrana" placeholder="Velicina ekrana"></span>
                <span class="span"><input type="text" name="brojBoja" placeholder="Broj boja"></span>
                <span class="span"><input type="text" name="rezolucija" placeholder="Rezolucija"></span>
                <span class="span"><input type="text" name="multitouch" placeholder="Multitouch"></span>
                <span class="span"><input type="text" name="zastita" placeholder="Zastita"></span>
                <span class="span"><input type="text" name="zvucnici" placeholder="Zvucnici"></span>
                <span class="span"><input type="text" name="melodije" placeholder="Melodije"></span>
                <span class="span"><input type="text" name="formatMelodija" placeholder="Format melodija"></span>
                <span class="span"><input type="text" name="spikerfon" placeholder="Spikerfon"></span>
                <span class="span"><input type="text" name="brojPiksela" placeholder="Broj piksela"></span>
                <span class="span"><input type="text" name="ostalo" placeholder="Ostalo"></span>
                <span class="span"><input type="text" name="video" placeholder="Video"></span>
                <span class="span"><input type="text" name="rezolucijaPomocneKamere" placeholder="Rezolucija pomocne kamere"></span>
                <span class="span"><input type="text" name="brojPikselaPomocneKamere" placeholder="Broj piksela pomocne kamere"></span>
                <span class="span"><input type="text" name="memorija" placeholder="Memorija"></span>
                <span class="span"><input type="text" name="podrzaniTipoviKartica" placeholder="Podrzani tipovi kartica"></span>
                <span class="span"><input type="text" name="grps" placeholder="Gprs"></span>
                <span class="span"><input type="text" name="edge" placeholder="Edge"></span>
                <span class="span"><input type="text" name="brzina" placeholder="Brzina"></span>
                <span class="span"><input type="text" name="wlan" placeholder="Wlan"></span>
                <span class="span"><input type="text" name="bluetoth" placeholder="Bluetoth"></span>
                <span class="span"><input type="text" name="nfc" placeholder="Nfc"></span>
                <span class="span"><input type="text" name="usb" placeholder="Usb"></span>
                <span class="span"><input type="text" name="operativniSistem" placeholder="Operativni sistem"></span>
                <span class="span"><input type="text" name="verzija" placeholder="Verzija"></span>
                <span class="span"><input type="text" name="procesor" placeholder="Procesor"></span>
                <span class="span"><input type="text" name="vrstaCipa" placeholder="Vrsta cipa"></span>
                <span class="span"><input type="text" name="grafickiProcesor" placeholder="Graficki procesor"></span>
                <span class="span"><input type="text" name="senzori" placeholder="Senzori"></span>
                <span class="span"><input type="text" name="formatiFajlova" placeholder="Formati fajlova"></span>
                
                
                </div>
                <div class="predkKrajAdministracija">
                    <input type="file" name="slika">
                </div>
                <div class="krajAdministracija">
                    <input type="submit" value="Dodaj mobilni u bazu">
                </div>
            </form>
                        <c:if test="${listaTelefona!=null}">
                        <div class="prikaz">
                        <table>
                            <tr>
                                <th>Id</th>
                                <th>Proizvođač</th>
                                <th>Naziv</th>
                                <th>Cena</th>
                            </tr>
                            <c:forEach items="${listaTelefona}" var="telefon">
                                <tr>
                                <td>${telefon.id}</td>
                                <td>${telefon.proizvodjac.naziv}</td>
                                <td>${telefon.naziv}</td>
                                <td>${telefon.cena}</td>
                                </tr>
                            </c:forEach>
                        </table>
                        </c:if>
                        <div class="PrikaziTelefone"><form method="post" action="prikaziTelefone"><input type="submit" value="Prikazi sve telefone"></form></div>
                        <div class="obrisiTelefon">
                            
                            <form method="post" action="obrisiTelefon">
                                <div id="obr1"><input type="text" name="id" placeholder="Id telefona"></div>
                                <div id="obr2"><input type="submit" value="Obrisi telefon"></div>
                            </form>
                        </div>
                        </div>
                    <div><form method="post" action="logout"><input type="submit" value="Logout"></form></div>
            </c:if>
            
            
        </div><!-- kraj diva sredina -->
        </div><!-- kraj diva osnovnog -->
    </body>
</html>