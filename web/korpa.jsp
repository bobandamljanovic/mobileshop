<%-- 
    Document   : korpa
    Created on : Jul 11, 2017, 6:43:45 PM
    Author     : Boban Damljanovic
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" rel="stylesheet" type="text/css">
        <title>Mobil Shop</title>
    </head>
    <body>
        <div id="osnovni"> <!-- pocetak osnovnog diva -->
            <div id="menu"> <!-- pocetak diva menu -->
                <a href="index.jsp"><img class="menuImg" src="slike/slikeStranice/logo.jpg" width="150px"></a>
                <a href="administracija.jsp"><p class="menuOpcije">Login</p></a>
                <a href="kontakt.jsp"><p class="menuOpcije">Kontakt</p></a>
                <a href="onama.jsp"><p class="menuOpcije">O nama</p></a>
                <a href="index.jsp"><p class="menuOpcije">Početna</p></a>
                
            </div> <!-- kraj diva menu -->
            
            <div id="pretraga"> <!-- pocetak diva pretraga -->
                <form method="get" action="pretraga">
                    <div class="pretragaOpcije">
                        <select name="proizvodjac">
                            <option value="0">Svi proizvođači</option>
                            <option value="1">Samsung</option>
                            <option value="2">Nokia</option>
                            <option value="3">Sony</option>
                            <option value="4">Apple</option>
                            <option value="5">LG</option>
                            <option value="6">HTC</option>
                            <option value="7">Microsoft</option>
                            <option value="8">Lenovo</option>
                            <option value="9">Huawei</option>
                        </select>
                    </div>
                    <div class="pretragaOpcije"><input type="text" size="13" placeholder="Minimalna cena" name="minCena"></div>
                    <div class="pretragaOpcije"><input type="text" size="13" placeholder="Maksimalna cena"name="maxCena"></div>
                <div class="pretragaOpcije"><input type="submit" value="Pretraga"></div>
                </form>
                <a href="korpa"><img src="slike/slikeStranice/korpa.png"></a>
            </div><!-- kraj diva pretraga -->
           
        <div id="sredina">
            <div id="proizvodjaci">
                <p class="izaberiP">Izaberite proizvođaca:</p>
                <a href="proizvodjac?id=1" ><p class="menuP"><img src="slike/slikeStranice/samsung.png"></p></a>
                <a href="proizvodjac?id=2" ><p class="menuP"><img src="slike/slikeStranice/nokia.png"></p></a>
                <a href="proizvodjac?id=3" ><p class="menuP"><img src="slike/slikeStranice/sony.png"></p></a>
                <a href="proizvodjac?id=4" ><p class="menuP"><img src="slike/slikeStranice/apple.png"></p></a>
                <a href="proizvodjac?id=5" ><p class="menuP"><img src="slike/slikeStranice/lg.png"></p></a>
                <a href="proizvodjac?id=6" ><p class="menuP"><img src="slike/slikeStranice/htc.png"></p></a>
                <a href="proizvodjac?id=7" ><p class="menuP"><img src="slike/slikeStranice/microsoft.png"></p></a>
                <a href="proizvodjac?id=8" ><p class="menuP"><img src="slike/slikeStranice/lenovo.png"></p></a>
                <a href="proizvodjac?id=9" ><p class="menuP"><img src="slike/slikeStranice/huawei.png"></p></a>
                
            </div><!-- kraj proizvdjaci diva -->
            <div id="telefoniPocetna">
                <c:if test="${poruka!=null}" >
                    <div><p class="greska">${poruka}</p><br><br></div>
                </c:if>
                
                <form method="post" action="upisPorudzbine">
                    <table>
                        <tr><th>Proizvođac</th><th>Model</th><th>Cena</th><th>Kolicina</th></tr>
                <c:forEach var="telefon" items="${listaTelefona}">
                    <input type="hidden" name="lista" value="${listaTelefona}">
                    <input type="hidden" name="idPorudzbina" value="${idPorudzbina}">
                        <tr>
                        <td>${telefon.proizvodjac.naziv}</td>
                        <td><a href="detalji?id=${telefon.id}"> ${telefon.naziv} </a> </td>
                        <td>${telefon.cena}€</td>
                        <td><input type="text" value="1" name="${telefon.naziv}" size="1"></td>
                        </tr>
                    
                </c:forEach>
                    </table>
                    <p class="greska">Popunite polja Vašim podacima:</p>
                    <p class="greska"><input type="text" size="10" placeholder="Ime:" name="ime"></p>
                    <p class="greska"><input type="text" size="10" placeholder="Prezime:" name="prezime"></p>
                    <p class="greska"><input type="text" size="10" placeholder="Adresa:" name="adresa"></p>
                    <p class="greska"><input type="text" size="10" placeholder="Grad:" name="grad"></p>
                    <p class="greska"><input type="text" size="10" placeholder="Broj telefona:" name="brojTelefona"></p>
                    <p class="greska"><input type="submit" value="Poruči">
                </form>
                
            </div><!-- kraj diva telefoniPocetna -->
            <div class="reklama"><br>
                <img src="slike/slikeStranice/reklama.png">
            </div><!-- kraj diva reklama -->
        </div><!-- kraj diva sredina -->
        </div><!-- kraj diva osnovnog -->
    </body>
</html>
