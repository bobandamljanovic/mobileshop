<%-- 
    Document   : index
    Created on : Jul 6, 2017, 7:54:36 PM
    Author     : Boban Damljanovic
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" rel="stylesheet" type="text/css">
        <title>Mobil Shop</title>
    </head>
    <body>
        <div id="osnovni"> <!-- pocetak osnovnog diva -->
            <div id="menu"> <!-- pocetak diva menu -->
                <a href="index.jsp"><img class="menuImg" src="slike/slikeStranice/logo.jpg" width="150px"></a>
                <a href="administracija.jsp"><p class="menuOpcije">Login</p></a>
                <a href="kontakt.jsp"><p class="menuOpcije">Kontakt</p></a>
                <a href="onama.jsp"><p class="menuOpcije">O nama</p></a>
                <a href="index.jsp"><p class="menuOpcije">Početna</p></a>
                
            </div> <!-- kraj diva menu -->
            
            <div id="pretraga"> <!-- pocetak diva pretraga -->
                <form method="get" action="pretraga">
                    <div class="pretragaOpcije">
                        <select name="proizvodjac">
                            <option value="0">Svi proizvođači</option>
                            <option value="1">Samsung</option>
                            <option value="2">Nokia</option>
                            <option value="3">Sony</option>
                            <option value="4">Apple</option>
                            <option value="5">LG</option>
                            <option value="6">HTC</option>
                            <option value="7">Microsoft</option>
                            <option value="8">Lenovo</option>
                            <option value="9">Huawei</option>
                        </select>
                    </div>
                    <div class="pretragaOpcije"><input type="text" size="13" placeholder="Minimalna cena" name="minCena"></div>
                    <div class="pretragaOpcije"><input type="text" size="13" placeholder="Maksimalna cena"name="maxCena"></div>
                <div class="pretragaOpcije"><input type="submit" value="Pretraga"></div>
                </form>
                <a href="korpa"><img src="slike/slikeStranice/korpa.png"></a>
            </div><!-- kraj diva pretraga -->
           
        <div id="sredina">
            <div id="proizvodjaci">
                <p class="izaberiP">Izaberite proizvođaca:</p>
                <a href="proizvodjac?id=1" ><p class="menuP"><img src="slike/slikeStranice/samsung.png"></p></a>
                <a href="proizvodjac?id=2" ><p class="menuP"><img src="slike/slikeStranice/nokia.png"></p></a>
                <a href="proizvodjac?id=3" ><p class="menuP"><img src="slike/slikeStranice/sony.png"></p></a>
                <a href="proizvodjac?id=4" ><p class="menuP"><img src="slike/slikeStranice/apple.png"></p></a>
                <a href="proizvodjac?id=5" ><p class="menuP"><img src="slike/slikeStranice/lg.png"></p></a>
                <a href="proizvodjac?id=6" ><p class="menuP"><img src="slike/slikeStranice/htc.png"></p></a>
                <a href="proizvodjac?id=7" ><p class="menuP"><img src="slike/slikeStranice/microsoft.png"></p></a>
                <a href="proizvodjac?id=8" ><p class="menuP"><img src="slike/slikeStranice/lenovo.png"></p></a>
                <a href="proizvodjac?id=9" ><p class="menuP"><img src="slike/slikeStranice/huawei.png"></p></a>
                
            </div><!-- kraj proizvdjaci diva -->
            <div id="telefoniPocetna">
                <c:if test="${poruka!=null}" >
                    <div><p class="greska">${poruka}</p><br><br></div>
                </c:if>
                
                <c:if test="${listaTelefona!=null}" >
                    <c:forEach var="telefon" items="${listaTelefona}">
                        <a href="detalji?id=${telefon.id}"><div class="telefoniDiv">
                        <img src="${telefon.slika}">
                        <p>${telefon.naziv}</p>
                        <p>${telefon.cena}€</p>
                        </div></a>
                    </c:forEach>
                </c:if>
                <c:if test="${listaTelefona==null}" >
                <a href="detalji?id=19"><div class="telefoniDiv">
                    <img src="slike/iphone/5S-16gb-grey.jpg">
                    <p>5s 64gb</p>
                    <p>360€</p>
                </div></a>
                <a href="detalji?id=6"><div class="telefoniDiv">
                    <img src="slike/nokia/lumia-625.jpg">
                    <p>Lumia 625</p>
                    <p>130€</p>
                </div></a>
                <a href="detalji?id=5"><div class="telefoniDiv">
                    <img src="slike/samsung/galaxy-a52.jpg">
                    <p>Galaxy A5</p>
                    <p>230€</p>
                    </div></a>
                </c:if>
                
            </div><!-- kraj diva telefoniPocetna -->
            <div class="reklama"><br>
                <img src="slike/slikeStranice/reklama.png">
            </div><!-- kraj diva reklama -->
        </div><!-- kraj diva sredina -->
        </div><!-- kraj diva osnovnog -->
    </body>
</html>