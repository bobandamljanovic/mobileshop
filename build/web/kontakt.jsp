<%-- 
    Document   : kontakt
    Created on : Jul 8, 2017, 11:59:42 AM
    Author     : Boban Damljanovic
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" rel="stylesheet" type="text/css">
        <title>Mobil Shop</title>
    </head>
    <body>
        <div id="osnovni"> <!-- pocetak osnovnog diva -->
            <div id="menu"> <!-- pocetak diva menu -->
                <a href="index.jsp"><img class="menuImg" src="slike/slikeStranice/logo.jpg" width="150px"></a>
                <a href="administracija.jsp"><p class="menuOpcije">Login</p></a>
                <a href="kontakt.jsp"><p class="menuOpcije">Kontakt</p></a>
                <a href="onama.jsp"><p class="menuOpcije">O nama</p></a>
                <a href="index.jsp"><p class="menuOpcije">Početna</p></a>
                
            </div> <!-- kraj diva menu -->
            
            <div id="pretraga"> <!-- pocetak diva pretraga -->
                <form method="get" action="pretraga">
                    <div class="pretragaOpcije">
                        <select name="proizvodjac">
                            <option value="0">Svi proizvođači</option>
                            <option value="1">Samsung</option>
                            <option value="2">Nokia</option>
                            <option value="3">Sony</option>
                            <option value="4">Apple</option>
                            <option value="5">LG</option>
                            <option value="6">HTC</option>
                            <option value="7">Microsoft</option>
                            <option value="8">Lenovo</option>
                            <option value="9">Huawei</option>
                        </select>
                    </div>
                    <div class="pretragaOpcije"><input type="text" size="13" placeholder="Minimalna cena" name="minCena"></div>
                    <div class="pretragaOpcije"><input type="text" size="13" placeholder="Maksimalna cena"name="maxCena"></div>
                <div class="pretragaOpcije"><input type="submit" value="Pretraga"></div>
                <a href="korpa"><img src="slike/slikeStranice/korpa.png"></a>
            </div><!-- kraj diva pretraga -->
           
        <div id="sredina">
            <div id="proizvodjaci">
                <p class="izaberiP">Izaberite proizvođaca:</p>
                <a href="proizvodjac?id=1" ><p class="menuP"><img src="slike/slikeStranice/samsung.png"></p></a>
                <a href="proizvodjac?id=2" ><p class="menuP"><img src="slike/slikeStranice/nokia.png"></p></a>
                <a href="proizvodjac?id=3" ><p class="menuP"><img src="slike/slikeStranice/sony.png"></p></a>
                <a href="proizvodjac?id=4" ><p class="menuP"><img src="slike/slikeStranice/apple.png"></p></a>
                <a href="proizvodjac?id=5" ><p class="menuP"><img src="slike/slikeStranice/lg.png"></p></a>
                <a href="proizvodjac?id=6" ><p class="menuP"><img src="slike/slikeStranice/htc.png"></p></a>
                <a href="proizvodjac?id=7" ><p class="menuP"><img src="slike/slikeStranice/microsoft.png"></p></a>
                <a href="proizvodjac?id=8" ><p class="menuP"><img src="slike/slikeStranice/lenovo.png"></p></a>
                <a href="proizvodjac?id=9" ><p class="menuP"><img src="slike/slikeStranice/huawei.png"></p></a>
                
            </div><!-- kraj proizvdjaci diva -->
            <div class="sredinaDiv">
                <div class="zaglavljeSredinaDiv">Kontakt</div>
                <div id="kontaktPodaci">
                    <b>Web prodaja</b><br>
                    <b>Telefon:</b> +381 65 123 456 ili +381 66 123 4567<br>
                    <b>E-mail:</b> kontakt@mobileshop.rs<br>
                    <b>Skype:</b> mobileShop<br>
                </div>
                <div class="posleZaglavljaSredine">
                    <b>Beograd radnja</b><br>
                    Balkanska 34<br>
                    +381 62 123 456<br>
                </div>
                <div class="posleZaglavljaSredine">
                    <b>Kragujevac radnja</b><br>
                    Makednoska 34<br>
                    +381 64 123 456<br>
                </div>
                <div class="posleZaglavljaSredine">
                    <b>Uzice radnja</b><br>
                    Zlatiborska 34<br>
                    +381 63 123 456<br>
                </div>
            </div><!-- kraj diva sredinaDiv -->
            <div class="reklama"><br>
                <img src="slike/slikeStranice/reklama.png">
            </div><!-- kraj diva reklama -->
        </div><!-- kraj diva sredina -->
        </div><!-- kraj diva osnovnog -->
    </body>
</html>
