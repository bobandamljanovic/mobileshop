<%-- 
    Document   : detalji
    Created on : Jul 8, 2017, 3:12:50 PM
    Author     : Boban Damljanovic
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" rel="stylesheet" type="text/css">
        <title>Mobil Shop</title>
    </head>
    <body>
        <div id="osnovni"> <!-- pocetak osnovnog diva -->
            <div id="menu"> <!-- pocetak diva menu -->
                <a href="index.jsp"><img class="menuImg" src="slike/slikeStranice/logo.jpg" width="150px"></a>
                <a href="administracija.jsp"><p class="menuOpcije">Login</p></a>
                <a href="kontakt.jsp"><p class="menuOpcije">Kontakt</p></a>
                <a href="onama.jsp"><p class="menuOpcije">O nama</p></a>
                <a href="index.jsp"><p class="menuOpcije">Početna</p></a>
                
            </div> <!-- kraj diva menu -->
            
            <div id="pretraga"> <!-- pocetak diva pretraga -->
                <form method="get" action="pretraga">
                    <div class="pretragaOpcije">
                        <select name="proizvodjac">
                            <option value="0">Svi proizvođači</option>
                            <option value="1">Samsung</option>
                            <option value="2">Nokia</option>
                            <option value="3">Sony</option>
                            <option value="4">Apple</option>
                            <option value="5">LG</option>
                            <option value="6">HTC</option>
                            <option value="7">Microsoft</option>
                            <option value="8">Lenovo</option>
                            <option value="9">Huawei</option>
                        </select>
                    </div>
                    <div class="pretragaOpcije"><input type="text" size="13" placeholder="Minimalna cena" name="minCena"></div>
                    <div class="pretragaOpcije"><input type="text" size="13" placeholder="Maksimalna cena"name="maxCena"></div>
                <div class="pretragaOpcije"><input type="submit" value="Pretraga"></div>
                </form>
                <a href="korpa"><img src="slike/slikeStranice/korpa.png"></a>
            </div><!-- kraj diva pretraga -->
           
        <div id="sredina">
            <div id="proizvodjaci">
                <p class="izaberiP">Izaberite proizvođaca:</p>
                <a href="proizvodjac?id=1" ><p class="menuP"><img src="slike/slikeStranice/samsung.png"></p></a>
                <a href="proizvodjac?id=2" ><p class="menuP"><img src="slike/slikeStranice/nokia.png"></p></a>
                <a href="proizvodjac?id=3" ><p class="menuP"><img src="slike/slikeStranice/sony.png"></p></a>
                <a href="proizvodjac?id=4" ><p class="menuP"><img src="slike/slikeStranice/apple.png"></p></a>
                <a href="proizvodjac?id=5" ><p class="menuP"><img src="slike/slikeStranice/lg.png"></p></a>
                <a href="proizvodjac?id=6" ><p class="menuP"><img src="slike/slikeStranice/htc.png"></p></a>
                <a href="proizvodjac?id=7" ><p class="menuP"><img src="slike/slikeStranice/microsoft.png"></p></a>
                <a href="proizvodjac?id=8" ><p class="menuP"><img src="slike/slikeStranice/lenovo.png"></p></a>
                <a href="proizvodjac?id=9" ><p class="menuP"><img src="slike/slikeStranice/huawei.png"></p></a>
                
            </div><!-- kraj proizvdjaci diva -->
            <div class="sredinaDiv">
                <div class="okolo">
                    <c:if test="${poruka!=null}">
                        <div><p class="greska">${poruka}</p><br><br></div>
                    </c:if>
                    
                <div id="DesnoDetalji">
                    <br>
                    <div class="DesnoDetaljiPodaci"><b>${telefon.naziv}</b></div>
                    <div class="donjaLinija"></div>
                    <div class="DesnoDetaljiPodaci"><b>${telefon.cena}€</b></div>
                    <div class="donjaLinija"></div>
                    <br>
                    <a href="dodajuKorpu?id=${telefon.id}"><img id="DodajUKorpu" src="slike/slikeStranice/ukorpu.jpg"></a>
                </div>
                <div id="LevoDetalji">
                    <img src="${telefon.slika}">
                </div>
                </div>
                <div id="DoleDetalji">
                    <table>
                        <tr>
                            <th>Osnovne karakteristike</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>2G</td>
                            <td>${telefon.detalji.dvaG}</td>
                        </tr>
                        <tr>
                            <td>3G</td>
                            <td>${telefon.detalji.triG}</td>
                        </tr>
                        <tr>
                            <td>4G</td>
                            <td>${telefon.detalji.cetiriG}</td>
                        </tr>
                        <tr>
                            <td>Proizvedeno</td>
                            <td>${telefon.detalji.proizvedeno}</td>
                        </tr>
                        <tr>
                            <td>Sim kartica</td>
                            <td>${telefon.detalji.simKartica}</td>
                        </tr>
                        <tr>
                            <td>Dimenzije</td>
                            <td>${telefon.detalji.dimenzije}</td>
                        </tr>
                        <tr>
                            <td>Masa</td>
                            <td>${telefon.detalji.masa}</td>
                        </tr>
                        <tr>
                            <td>Tastatura</td>
                            <td>${telefon.detalji.tastatura}</td>
                        </tr>
                        <tr>
                            <th>Baterija</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>Tip baterije</td>
                            <td>${telefon.detalji.tipBaterije}</td>
                        </tr>
                        <tr>
                            <th>Glavni ekran</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>Tip ekrana</td>
                            <td>${telefon.detalji.tipEkrana}</td>
                        </tr>
                        <tr>
                            <td>Veličina ekrana</td>
                            <td>${telefon.detalji.velicinaEkrana}</td>
                        </tr>
                        <tr>
                            <td>Broj boja</td>
                            <td>${telefon.detalji.brojBoja}</td>
                        </tr>
                        <tr>
                            <td>Rezolucija</td>
                            <td>${telefon.detalji.rezolucija}</td>
                        </tr>
                        <tr>
                            <td>Multitouch</td>
                            <td>${telefon.detalji.multitouch}</td>
                        </tr>
                        <tr>
                            <td>Zaštita</td>
                            <td>${telefon.detalji.zastita}</td>
                        </tr>
                        <tr>
                            <th>Zvuk</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>Zvučnici</td>
                            <td>${telefon.detalji.zvucnici}</td>
                        </tr>
                        <tr>
                            <td>Melodije</td>
                            <td>${telefon.detalji.melodije}</td>
                        </tr>
                        <tr>
                            <td>Format melodija</td>
                            <td>${telefon.detalji.formatMelodija}</td>
                        </tr>
                        <tr>
                            <td>Spikerfon</td>
                            <td>${telefon.detalji.spikerfon}</td>
                        </tr>
                        <tr>
                            <th>Glavna kamera</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>Broj piksela</td>
                            <td>${telefon.detalji.brojPiksela}</td>
                        </tr>
                        <tr>
                            <td>Ostalo</td>
                            <td>${telefon.detalji.ostalo}</td>
                        </tr>
                        <tr>
                            <td>Video</td>
                            <td>${telefon.detalji.video}</td>
                        </tr>
                        <tr>
                            <th>Pomocna kamera</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>Rezolucija</td>
                            <td>${telefon.detalji.rezolucija}</td>
                        </tr>
                        <tr>
                            <td>Broj piksela</td>
                            <td>${telefon.detalji.brojPikselaPomocneKamere}</td>
                        </tr>
                        <tr>
                            <th>Memorija telefona</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>Memorija</td>
                            <td>${telefon.detalji.memorija}</td>
                        </tr>
                        <tr>
                            <td>Podržani tipovi kartica</td>
                            <td>${telefon.detalji.podrzaniTipoviKartica}</td>
                        </tr>
                        <tr>
                            <th>Prenos podataka</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>Gprs</td>
                            <td>${telefon.detalji.grps}</td>
                        </tr>
                        <tr>
                            <td>Edge</td>
                            <td>${telefon.detalji.edge}</td>
                        </tr>
                        <tr>
                            <td>Brzina</td>
                            <td>${telefon.detalji.brzina}</td>
                        </tr>
                        <tr>
                            <td>Wlan</td>
                            <td>${telefon.detalji.wlan}</td>
                        </tr>
                        <tr>
                            <td>Bluetoth</td>
                            <td>${telefon.detalji.bluetoth}</td>
                        </tr>
                        <tr>
                            <td>Nfc</td>
                            <td>${telefon.detalji.nfc}</td>
                        </tr>
                        <tr>
                            <td>Usb</td>
                            <td>${telefon.detalji.usb}</td>
                        </tr>
                        <tr>
                            <th>Softwer</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>Operativni sistem</td>
                            <td>${telefon.detalji.operativniSistem}</td>
                        </tr>
                        <tr>
                            <td>Verzija</td>
                            <td>${telefon.detalji.verzija}</td>
                        </tr>
                        <tr>
                            <td>Procesor</td>
                            <td>${telefon.detalji.procesor}</td>
                        </tr>
                        <tr>
                            <td>Vrsta čipa</td>
                            <td>${telefon.detalji.vrstaCipa}</td>
                        </tr>
                        <tr>
                            <td>Grafički procesor</td>
                            <td>${telefon.detalji.grafickiProcesor}</td>
                        </tr>
                        <tr>
                            <td>Senzori</td>
                            <td>${telefon.detalji.senzori}</td>
                        </tr>
                        <tr>
                            <td>Formati fajlova</td>
                            <td>${telefon.detalji.formatiFajlova}</td>
                        </tr>
                    </table>
                </div>
            </div><!-- kraj diva sredinaDiv -->
            <div class="reklama"><br>
                <img src="slike/slikeStranice/reklama.png">
            </div><!-- kraj diva reklama -->
        </div><!-- kraj diva sredina -->
        </div><!-- kraj diva osnovni -->
    </body>
</html>