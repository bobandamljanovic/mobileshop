package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class Telefon implements Serializable {

    

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String naziv;
    private int cena;
    private String slika;
    private int kolicina;
    
    @ManyToOne(targetEntity = Proizvodjac.class)
    private Proizvodjac proizvodjac;
    
    
    @OneToOne(targetEntity = Detalji.class)
    @PrimaryKeyJoinColumn
    private Detalji detalji;
    
    @ManyToMany
    @JoinTable( name = "porudzbineTelefoni",
            joinColumns = {@JoinColumn(name = "telefon")},
            inverseJoinColumns = {@JoinColumn(name = "porudzbina")}
    )
    private List<Porudzbina> porudzbine;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    /**
     * @return the naziv
     */
    public String getNaziv() {
        return naziv;
    }

    /**
     * @param naziv the naziv to set
     */
    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    /**
     * @return the cena
     */
    public int getCena() {
        return cena;
    }

    /**
     * @param cena the cena to set
     */
    public void setCena(int cena) {
        this.cena = cena;
    }

    /**
     * @return the proizvodjac
     */
    public Proizvodjac getProizvodjac() {
        return proizvodjac;
    }

    /**
     * @param proizvodjac the proizvodjac to set
     */
    public void setProizvodjac(Proizvodjac proizvodjac) {
        this.proizvodjac = proizvodjac;
    }

    /**
     * @return the detalji
     */
    public Detalji getDetalji() {
        return detalji;
    }

    /**
     * @param detalji the detalji to set
     */
    public void setDetalji(Detalji detalji) {
        this.detalji = detalji;
    }

    /**
     * @return the slika
     */
    public String getSlika() {
        return slika;
    }

    /**
     * @param slika the slika to set
     */
    public void setSlika(String slika) {
        this.slika = slika;
    }

    /**
     * @return the kolicina
     */
    public int getKolicina() {
        return kolicina;
    }

    /**
     * @param kolicina the kolicina to set
     */
    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    /**
     * @return the porudzbine
     */
    public List<Porudzbina> getPorudzbine() {
        return porudzbine;
    }

    /**
     * @param porudzbine the porudzbine to set
     */
    public void setPorudzbine(List<Porudzbina> porudzbine) {
        this.porudzbine = porudzbine;
    }

}
