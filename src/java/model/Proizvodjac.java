package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Proizvodjac implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    private String naziv;
    @OneToMany(targetEntity = Telefon.class,mappedBy = "proizvodjac")
    private List<Telefon> listaTelefona;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the naziv
     */
    public String getNaziv() {
        return naziv;
    }

    /**
     * @param naziv the naziv to set
     */
    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    /**
     * @return the listaTelefona
     */
    public List<Telefon> getListaTelefona() {
        return listaTelefona;
    }

    /**
     * @param listaTelefona the listaTelefona to set
     */
    public void setListaTelefona(List<Telefon> listaTelefona) {
        this.listaTelefona = listaTelefona;
    }

}
