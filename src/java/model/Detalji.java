package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
public class Detalji implements Serializable {

    @Id
    @GenericGenerator(name = "generator",strategy = "foreign",parameters = @Parameter(name = "property",value = "telefon"))
    @GeneratedValue (generator = "generator")
    private int id;
    
    @OneToOne
    @PrimaryKeyJoinColumn
    private Telefon telefon;
    
        private String dvaG;
        private String triG;
        private String cetiriG;
        private String proizvedeno;
        private String simKartica;
        private String dimenzije;
        private String masa;
        private String tastatura;
        private String tipBaterije;
        private String tipEkrana;
        private String velicinaEkrana;
        private String brojBoja;
        private String rezolucija;
        private String multitouch;
        private String zastita;
        private String zvucnici;
        private String melodije;
        private String formatMelodija;
        private String spikerfon;
        private String brojPiksela;
        private String ostalo;
        private String video;
        private String rezolucijaPomocneKamere;
        private String brojPikselaPomocneKamere;
        private String memorija;
        private String podrzaniTipoviKartica;
        private String grps;
        private String edge;
        private String brzina;
        private String wlan;
        private String bluetoth;
        private String nfc;
        private String usb;
        private String operativniSistem;
        private String verzija;
        private String procesor;
        private String vrstaCipa;
        private String grafickiProcesor;
        private String senzori;
        private String formatiFajlova;
        
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the telefon
     */
    public Telefon getTelefon() {
        return telefon;
    }

    /**
     * @param telefon the telefon to set
     */
    public void setTelefon(Telefon telefon) {
        this.telefon = telefon;
    }

    /**
     * @return the dvaG
     */
    public String getDvaG() {
        return dvaG;
    }

    /**
     * @param dvaG the dvaG to set
     */
    public void setDvaG(String dvaG) {
        this.dvaG = dvaG;
    }

    /**
     * @return the triG
     */
    public String getTriG() {
        return triG;
    }

    /**
     * @param triG the triG to set
     */
    public void setTriG(String triG) {
        this.triG = triG;
    }

    /**
     * @return the cetiriG
     */
    public String getCetiriG() {
        return cetiriG;
    }

    /**
     * @param cetiriG the cetiriG to set
     */
    public void setCetiriG(String cetiriG) {
        this.cetiriG = cetiriG;
    }

    /**
     * @return the proizvedeno
     */
    public String getProizvedeno() {
        return proizvedeno;
    }

    /**
     * @param proizvedeno the proizvedeno to set
     */
    public void setProizvedeno(String proizvedeno) {
        this.proizvedeno = proizvedeno;
    }

    /**
     * @return the simKartica
     */
    public String getSimKartica() {
        return simKartica;
    }

    /**
     * @param simKartica the simKartica to set
     */
    public void setSimKartica(String simKartica) {
        this.simKartica = simKartica;
    }

    /**
     * @return the dimenzije
     */
    public String getDimenzije() {
        return dimenzije;
    }

    /**
     * @param dimenzije the dimenzije to set
     */
    public void setDimenzije(String dimenzije) {
        this.dimenzije = dimenzije;
    }

    /**
     * @return the masa
     */
    public String getMasa() {
        return masa;
    }

    /**
     * @param masa the masa to set
     */
    public void setMasa(String masa) {
        this.masa = masa;
    }

    /**
     * @return the tastatura
     */
    public String getTastatura() {
        return tastatura;
    }

    /**
     * @param tastatura the tastatura to set
     */
    public void setTastatura(String tastatura) {
        this.tastatura = tastatura;
    }

    /**
     * @return the tipBaterije
     */
    public String getTipBaterije() {
        return tipBaterije;
    }

    /**
     * @param tipBaterije the tipBaterije to set
     */
    public void setTipBaterije(String tipBaterije) {
        this.tipBaterije = tipBaterije;
    }

    /**
     * @return the tipEkrana
     */
    public String getTipEkrana() {
        return tipEkrana;
    }

    /**
     * @param tipEkrana the tipEkrana to set
     */
    public void setTipEkrana(String tipEkrana) {
        this.tipEkrana = tipEkrana;
    }

    /**
     * @return the velicinaEkrana
     */
    public String getVelicinaEkrana() {
        return velicinaEkrana;
    }

    /**
     * @param velicinaEkrana the velicinaEkrana to set
     */
    public void setVelicinaEkrana(String velicinaEkrana) {
        this.velicinaEkrana = velicinaEkrana;
    }

    /**
     * @return the brojBoja
     */
    public String getBrojBoja() {
        return brojBoja;
    }

    /**
     * @param brojBoja the brojBoja to set
     */
    public void setBrojBoja(String brojBoja) {
        this.brojBoja = brojBoja;
    }

    /**
     * @return the rezolucija
     */
    public String getRezolucija() {
        return rezolucija;
    }

    /**
     * @param rezolucija the rezolucija to set
     */
    public void setRezolucija(String rezolucija) {
        this.rezolucija = rezolucija;
    }

    /**
     * @return the multitouch
     */
    public String getMultitouch() {
        return multitouch;
    }

    /**
     * @param multitouch the multitouch to set
     */
    public void setMultitouch(String multitouch) {
        this.multitouch = multitouch;
    }

    /**
     * @return the zastita
     */
    public String getZastita() {
        return zastita;
    }

    /**
     * @param zastita the zastita to set
     */
    public void setZastita(String zastita) {
        this.zastita = zastita;
    }

    /**
     * @return the zvucnici
     */
    public String getZvucnici() {
        return zvucnici;
    }

    /**
     * @param zvucnici the zvucnici to set
     */
    public void setZvucnici(String zvucnici) {
        this.zvucnici = zvucnici;
    }

    /**
     * @return the melodije
     */
    public String getMelodije() {
        return melodije;
    }

    /**
     * @param melodije the melodije to set
     */
    public void setMelodije(String melodije) {
        this.melodije = melodije;
    }

    /**
     * @return the formatMelodija
     */
    public String getFormatMelodija() {
        return formatMelodija;
    }

    /**
     * @param formatMelodija the formatMelodija to set
     */
    public void setFormatMelodija(String formatMelodija) {
        this.formatMelodija = formatMelodija;
    }

    /**
     * @return the spikerfon
     */
    public String getSpikerfon() {
        return spikerfon;
    }

    /**
     * @param spikerfon the spikerfon to set
     */
    public void setSpikerfon(String spikerfon) {
        this.spikerfon = spikerfon;
    }

    /**
     * @return the brojPiksela
     */
    public String getBrojPiksela() {
        return brojPiksela;
    }

    /**
     * @param brojPiksela the brojPiksela to set
     */
    public void setBrojPiksela(String brojPiksela) {
        this.brojPiksela = brojPiksela;
    }

    /**
     * @return the ostalo
     */
    public String getOstalo() {
        return ostalo;
    }

    /**
     * @param ostalo the ostalo to set
     */
    public void setOstalo(String ostalo) {
        this.ostalo = ostalo;
    }

    /**
     * @return the video
     */
    public String getVideo() {
        return video;
    }

    /**
     * @param video the video to set
     */
    public void setVideo(String video) {
        this.video = video;
    }

    /**
     * @return the rezolucijaPomocneKamere
     */
    public String getRezolucijaPomocneKamere() {
        return rezolucijaPomocneKamere;
    }

    /**
     * @param rezolucijaPomocneKamere the rezolucijaPomocneKamere to set
     */
    public void setRezolucijaPomocneKamere(String rezolucijaPomocneKamere) {
        this.rezolucijaPomocneKamere = rezolucijaPomocneKamere;
    }

    /**
     * @return the brojPikselaPomocneKamere
     */
    public String getBrojPikselaPomocneKamere() {
        return brojPikselaPomocneKamere;
    }

    /**
     * @param brojPikselaPomocneKamere the brojPikselaPomocneKamere to set
     */
    public void setBrojPikselaPomocneKamere(String brojPikselaPomocneKamere) {
        this.brojPikselaPomocneKamere = brojPikselaPomocneKamere;
    }

    /**
     * @return the memorija
     */
    public String getMemorija() {
        return memorija;
    }

    /**
     * @param memorija the memorija to set
     */
    public void setMemorija(String memorija) {
        this.memorija = memorija;
    }

    /**
     * @return the podrzaniTipoviKartica
     */
    public String getPodrzaniTipoviKartica() {
        return podrzaniTipoviKartica;
    }

    /**
     * @param podrzaniTipoviKartica the podrzaniTipoviKartica to set
     */
    public void setPodrzaniTipoviKartica(String podrzaniTipoviKartica) {
        this.podrzaniTipoviKartica = podrzaniTipoviKartica;
    }

    /**
     * @return the grps
     */
    public String getGrps() {
        return grps;
    }

    /**
     * @param grps the grps to set
     */
    public void setGrps(String grps) {
        this.grps = grps;
    }

    /**
     * @return the edge
     */
    public String getEdge() {
        return edge;
    }

    /**
     * @param edge the edge to set
     */
    public void setEdge(String edge) {
        this.edge = edge;
    }

    /**
     * @return the brzina
     */
    public String getBrzina() {
        return brzina;
    }

    /**
     * @param brzina the brzina to set
     */
    public void setBrzina(String brzina) {
        this.brzina = brzina;
    }

    /**
     * @return the wlan
     */
    public String getWlan() {
        return wlan;
    }

    /**
     * @param wlan the wlan to set
     */
    public void setWlan(String wlan) {
        this.wlan = wlan;
    }

    /**
     * @return the bluetoth
     */
    public String getBluetoth() {
        return bluetoth;
    }

    /**
     * @param bluetoth the bluetoth to set
     */
    public void setBluetoth(String bluetoth) {
        this.bluetoth = bluetoth;
    }

    /**
     * @return the nfc
     */
    public String getNfc() {
        return nfc;
    }

    /**
     * @param nfc the nfc to set
     */
    public void setNfc(String nfc) {
        this.nfc = nfc;
    }

    /**
     * @return the usb
     */
    public String getUsb() {
        return usb;
    }

    /**
     * @param usb the usb to set
     */
    public void setUsb(String usb) {
        this.usb = usb;
    }

    /**
     * @return the operativniSistem
     */
    public String getOperativniSistem() {
        return operativniSistem;
    }

    /**
     * @param operativniSistem the operativniSistem to set
     */
    public void setOperativniSistem(String operativniSistem) {
        this.operativniSistem = operativniSistem;
    }

    /**
     * @return the verzija
     */
    public String getVerzija() {
        return verzija;
    }

    /**
     * @param verzija the verzija to set
     */
    public void setVerzija(String verzija) {
        this.verzija = verzija;
    }

    /**
     * @return the procesor
     */
    public String getProcesor() {
        return procesor;
    }

    /**
     * @param procesor the procesor to set
     */
    public void setProcesor(String procesor) {
        this.procesor = procesor;
    }

    /**
     * @return the vrstaCipa
     */
    public String getVrstaCipa() {
        return vrstaCipa;
    }

    /**
     * @param vrstaCipa the vrstaCipa to set
     */
    public void setVrstaCipa(String vrstaCipa) {
        this.vrstaCipa = vrstaCipa;
    }

    /**
     * @return the grafickiProcesor
     */
    public String getGrafickiProcesor() {
        return grafickiProcesor;
    }

    /**
     * @param grafickiProcesor the grafickiProcesor to set
     */
    public void setGrafickiProcesor(String grafickiProcesor) {
        this.grafickiProcesor = grafickiProcesor;
    }

    /**
     * @return the senzori
     */
    public String getSenzori() {
        return senzori;
    }

    /**
     * @param senzori the senzori to set
     */
    public void setSenzori(String senzori) {
        this.senzori = senzori;
    }

    /**
     * @return the formatiFajlova
     */
    public String getFormatiFajlova() {
        return formatiFajlova;
    }

    /**
     * @param formatiFajlova the formatiFajlova to set
     */
    public void setFormatiFajlova(String formatiFajlova) {
        this.formatiFajlova = formatiFajlova;
    }
}