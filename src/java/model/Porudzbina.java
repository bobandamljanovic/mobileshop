package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Porudzbina implements Serializable {

    @Id
    private Long id;
    
    private String ime;
    private String prezime;
    private String adresa;
    private String grad;
    private String brojTelefon;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable( name = "porudzbineTelefoni",
            joinColumns = {@JoinColumn(name = "porudzbina")},
            inverseJoinColumns = {@JoinColumn(name = "telefon")}
    )
    private List<Telefon> telefoni;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the ime
     */
    public String getIme() {
        return ime;
    }

    /**
     * @param ime the ime to set
     */
    public void setIme(String ime) {
        this.ime = ime;
    }

    /**
     * @return the prezime
     */
    public String getPrezime() {
        return prezime;
    }

    /**
     * @param prezime the prezime to set
     */
    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    /**
     * @return the adresa
     */
    public String getAdresa() {
        return adresa;
    }

    /**
     * @param adresa the adresa to set
     */
    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    /**
     * @return the grad
     */
    public String getGrad() {
        return grad;
    }

    /**
     * @param grad the grad to set
     */
    public void setGrad(String grad) {
        this.grad = grad;
    }

    /**
     * @return the brojTelefon
     */
    public String getBrojTelefon() {
        return brojTelefon;
    }

    /**
     * @param brojTelefon the brojTelefon to set
     */
    public void setBrojTelefon(String brojTelefon) {
        this.brojTelefon = brojTelefon;
    }

    /**
     * @return the telefoni
     */
    public List<Telefon> getTelefoni() {
        return telefoni;
    }

    /**
     * @param telefoni the telefoni to set
     */
    public void setTelefoni(List<Telefon> telefoni) {
        this.telefoni = telefoni;
    }

}
