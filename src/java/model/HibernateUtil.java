package model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
        private static SessionFactory sessionFactory;
        private static ServiceRegistry serviceRegistry;
    
    public static SessionFactory getSessionFactory(){
        if(sessionFactory==null||sessionFactory.isClosed()){
            Configuration config = new Configuration();
            config.configure();
            serviceRegistry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
            sessionFactory = config.buildSessionFactory(serviceRegistry);
        }
        return sessionFactory;
    }
    public static List<Telefon> getByProizvodjacId(int id){
        Session sesija = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = sesija.beginTransaction();
        if(id==0){
            Query q = sesija.createQuery("from Telefon as telefon");
            Proizvodjac p = new Proizvodjac();
            p.setListaTelefona(q.list());
            tx.commit();
            sesija.close();
            return p.getListaTelefona();
        }else{
            Query q = sesija.createQuery("from Telefon as telefon where telefon.proizvodjac.id = :id");
            q.setParameter("id",id);
            Proizvodjac p = new Proizvodjac();
            p.setListaTelefona(q.list());
            tx.commit();
            sesija.close();
            return p.getListaTelefona();
        }
    }
    public static List<Telefon> getAllTelefon(){
        Session sesija = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = sesija.beginTransaction();
        Query q = sesija.createQuery("from Telefon as telefon");
        List<Telefon> t =q.list();
        tx.commit();
        sesija.close();
        return t;
    }
    public static void deleleTelefon(int id){
        Session sesija = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = sesija.beginTransaction();
        Query q = sesija.createQuery("from Telefon as telefon where telefon.id = :id");
        q.setParameter("id",id);
        Telefon t = (Telefon) q.list().get(0);
        sesija.delete(t);
        tx.commit();
        sesija.close();
    }
    public static Telefon getTelefonById(int id){
        
        Session sesija = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = sesija.beginTransaction();
        Query q = sesija.createQuery("from Telefon as telefon where telefon.id = :id");
        q.setParameter("id",id);
        Telefon t = (Telefon) q.list().get(0);
        tx.commit();
        sesija.close();
        return t;
    }
    
    public static List<Telefon> getTelefoneByParametar(int id,int min, int max){
        Session sesija = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = sesija.beginTransaction();
        if(id==0){
            Query q = sesija.createQuery("from Telefon as telefon where telefon.cena > :min and telefon.cena < :max");
            q.setParameter("min", min);
            q.setParameter("max", max);
            List<Telefon> listaTelefona = q.list();
            tx.commit();
            sesija.close();
            return listaTelefona;
        }else{
                Query q = sesija.createQuery("from Telefon as telefon where telefon.proizvodjac.id = :id and telefon.cena > :min and telefon.cena < :max");
                q.setParameter("id", id);
                q.setParameter("min", min);
                q.setParameter("max", max);
                List<Telefon> listaTelefona = q.list();
                tx.commit();
                sesija.close();
                return listaTelefona;
        }
    }
       
    
   public static void savePorudzbina(Porudzbina porudzbina){
       Session sesija = HibernateUtil.getSessionFactory().openSession();
       Transaction tx = sesija.beginTransaction();
       sesija.save(porudzbina);
       tx.commit();
       sesija.close(); 
   }
   public static Porudzbina getPorudzbinaById(long id){
        
        Session sesija = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = sesija.beginTransaction();
        Query q = sesija.createQuery("from Porudzbina as porudzbina where porudzbina.id = :id");
        q.setParameter("id",id);
        Porudzbina p = (Porudzbina) q.list().get(0);
        tx.commit();
        sesija.close();
        return p;
        
    }
   public static void addTelefonToPorudzbina(int telefonID,long porudzbinaID){
       Session sesija = HibernateUtil.getSessionFactory().openSession();
       Transaction tx = sesija.beginTransaction();
       Query q = sesija.createQuery("from Porudzbina as porudzbina where porudzbina.id = :id");
       q.setParameter("id",porudzbinaID);
       Porudzbina p = (Porudzbina) q.list().get(0);
       Query q2 = sesija.createQuery("from Telefon as telefon where telefon.id = :id");
       q2.setParameter("id",telefonID);
       Telefon t = (Telefon) q2.list().get(0);
       p.getTelefoni().add(t);
       sesija.update(p);
       tx.commit();
       sesija.close();
   }
   public static List<Telefon> getListaTelefonaFromPorudzbina(long porudzbina){
       
       Session sesija = HibernateUtil.getSessionFactory().openSession();
       Transaction tx = sesija.beginTransaction();
       Query q = sesija.createQuery("from Porudzbina as porudzbina where porudzbina.id = :id");
       q.setParameter("id",porudzbina);
       Porudzbina p = (Porudzbina) q.list().get(0);
       List<Telefon> lista = p.getTelefoni();
       tx.commit();
       sesija.close();
       return lista;
   }
   public static String updatePorudzbina(long porudzbinaID,String ime,String prezime, String grad,String brojTelefona,String adresa){
       try{
       Session sesija = HibernateUtil.getSessionFactory().openSession();
       Transaction tx = sesija.beginTransaction();
       Query q = sesija.createQuery("from Porudzbina as porudzbina where porudzbina.id = :id");
       q.setParameter("id",porudzbinaID);
       Porudzbina p = (Porudzbina) q.list().get(0);
       p.setAdresa(adresa);
       p.setBrojTelefon(brojTelefona);
       p.setIme(ime);
       p.setPrezime(prezime);
       p.setGrad(grad);
       sesija.update(p);
       tx.commit();
       sesija.close();
       return "ok";
       }catch(Exception e){
           return "error";
       }
        
   }
}
