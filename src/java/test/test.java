/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Detalji;
import model.HibernateUtil;
import model.Proizvodjac;
import model.Telefon;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author FCCZ
 */
@WebServlet(name = "test", urlPatterns = {"/test"})
public class test extends HttpServlet {
    public static void main(String[] args) {
        Session sesija = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = sesija.beginTransaction();
//        Query q = sesija.createQuery("from Proizvodjac as proizvodjac where proizvodjac.id = 9");
//        Proizvodjac p =(Proizvodjac) q.list().get(0);
//        
//        Detalji d = new Detalji();
//        d.setDvaG("GSM 850 / 900 / 1800 / 1900");
//        d.setTriG("HSDPA 850 / 900 / 1900 / 2100");
//        d.setCetiriG("LTE band 1(2100), 3(1800), 5(850), 7(2600), 8(900), 20(800), 40(2300)");
//        d.setProizvedeno("januar, 2017. godina");
//        d.setSimKartica("Dual SIM");
//        d.setDimenzije("154 x 58.1 x 7.1 mm");
//        d.setMasa("192 grama");
//        d.setTastatura("nema,touchscreen");
//        d.setTipBaterije("Li-Po 3900 mAh");
//        d.setTipEkrana("LED-backlit IPS LCD,osetljiv na dodir");
//        d.setVelicinaEkrana("5.0 inca");
//        d.setBrojBoja("(24-bit) 16.777.216 boja");
//        d.setRezolucija("4200 x 26000 piksela");
//        d.setMultitouch("da");
//        d.setZastita("Corning Gorilla Glass 6,Optimus UX 4.0 UI");
//        d.setZvucnici("stereo");
//        d.setMelodije("polifone");
//        d.setFormatMelodija("mp3, wav");
//        d.setSpikerfon("da");
//        d.setBrojPiksela("16 megapiksela");
//        d.setOstalo("video zapis, blic, autofokus, 1/3'' 8");
//        d.setVideo("2160p@30fps, 1080p@60fps,1080p@120fps, 720p@240fps");
//        d.setRezolucijaPomocneKamere("5 megapiksela za video pozive");
//        d.setBrojPikselaPomocneKamere("/");
//        d.setMemorija("16 GB RAM DDR3");
//        d.setPodrzaniTipoviKartica("microSD (Trans Flash),do 16GB");
//        d.setGrps("da");
//        d.setEdge("da");
//        d.setBrzina("HSPA 42.2/5.76 Mbps, LTE Cat4 150/50 Mbps or LTE Cat6 300/50 Mbps");
//        d.setWlan("Wi-Fi 802.11 b/g/n, Wi-Fi Direct, hotspot");
//        d.setBluetoth("v4.0, A2DP, EDR, LE");
//        d.setNfc("da");
//        d.setUsb("microUSB v2.0");
//        d.setOperativniSistem("Android");
//        d.setVerzija("v5.0.1 (Lollipop), upgradable to v5.1.1 (Lollipop)");
//        d.setVrstaCipa("Quad-core 1.5 GHz Cortex-A53 & Quad-core 2 GHz Cortex-A57");
//        d.setGrafickiProcesor("PowerVR G6430 (quad-core graphics)");
//        d.setSenzori("accelerometer, gyro, proximity, compass");
//        d.setFormatiFajlova("aac, wav, mp3, mp4, flac, XviD, H.265");
        
//        
//        Telefon t = HibernateUtil.getTelefonById(26);
//        
//         
//        sesija.update(t);
        
        tx.commit();
        sesija.close();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet test</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet test at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
