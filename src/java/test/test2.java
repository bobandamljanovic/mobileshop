package test;

import model.HibernateUtil;
import model.Proizvodjac;
import model.Telefon;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class test2 {
    public static void main(String[] args) {
        Session sesija = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = sesija.beginTransaction();
        Query q = sesija.createQuery("from Telefon as telefon where telefon.proizvodjac.id = 1");
        for (Object list : q.list()) {
            Telefon t = (Telefon) list;
            System.out.println(t.getDetalji().getMasa()+" "+t.getProizvodjac().getNaziv()+" "+t.getNaziv());
    
        }
        
        tx.commit();
        sesija.close();
    }
}
