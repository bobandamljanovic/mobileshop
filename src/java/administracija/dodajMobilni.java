/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package administracija;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import model.Detalji;
import model.HibernateUtil;
import model.Proizvodjac;
import model.Telefon;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author FCCZ
 */
@WebServlet(name = "dodajMobilni", urlPatterns = {"/dodajMobilni"})
@MultipartConfig
public class dodajMobilni extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try{
           Part part = request.getPart("slika");
           String galleryPath = "C:\\Users\\FCCZ\\Documents\\NetBeansProjects\\MobilShop\\web\\slike";
           String fileName = part.getHeader("content-Disposition").toString().split("filename=\"")[1].replace("\"", "");
           InputStream is = part.getInputStream();
           String proizvodjac = request.getParameter("proizvodjac");
           FileOutputStream fos = new FileOutputStream(galleryPath+"/"+proizvodjac+"/"+fileName);
           int bt;
           while((bt=is.read())!=-1){
            fos.write(bt);
                }
           fos.close();
           is.close();
        Session sesija = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = sesija.beginTransaction();
        
            Query q = sesija.createQuery("from Proizvodjac as proizvodjac where proizvodjac.id = :id");
        switch(proizvodjac){
            case "samsung":
              q.setParameter("id",1);
              break;
              case "nokia":
              q.setParameter("id",2); 
              break;
              case "sony":
              q.setParameter("id",3); 
              break;
              case "iphone":
              q.setParameter("id",4); 
              break;
              case "lg":
              q.setParameter("id",5); 
              break;
              case "htc":
              q.setParameter("id",6); 
              break;
              case "microsoft":
              q.setParameter("id",7); 
              break;
              case "lenovo":
              q.setParameter("id",8);
              break;
              case "huawei":
              q.setParameter("id",9); 
              break;
              
        }
        
        Proizvodjac p =(Proizvodjac) q.list().get(0);
        
        Detalji d = new Detalji();
        d.setDvaG(request.getParameter("dvaG"));
        d.setTriG(request.getParameter("triG"));
        d.setCetiriG(request.getParameter("cetiriG"));
        d.setProizvedeno(request.getParameter("proizvedeno"));
        d.setSimKartica(request.getParameter("simKartica"));
        d.setDimenzije(request.getParameter("dimenzije"));
        d.setMasa(request.getParameter("masa"));
        d.setTastatura(request.getParameter("tastatura"));
        d.setTipBaterije(request.getParameter("tipBaterije"));
        d.setTipEkrana(request.getParameter("tipEkrana"));
        d.setVelicinaEkrana(request.getParameter("velicinaEkrana"));
        d.setBrojBoja(request.getParameter("brojBoja"));
        d.setRezolucija(request.getParameter("rezolucija"));
        d.setMultitouch(request.getParameter("multitouch"));
        d.setZastita(request.getParameter("zastita"));
        d.setZvucnici(request.getParameter("zvucnici"));
        d.setMelodije(request.getParameter("melodije"));
        d.setFormatMelodija(request.getParameter("formatMelodija"));
        d.setSpikerfon(request.getParameter("spikerfon"));
        d.setBrojPiksela(request.getParameter("brojPiksela"));
        d.setOstalo(request.getParameter("ostalo"));
        d.setVideo(request.getParameter("video"));
        d.setRezolucijaPomocneKamere(request.getParameter("rezolucijaPomocneKamere"));
        d.setBrojPikselaPomocneKamere(request.getParameter("brojPikselaPomocneKamere"));
        d.setMemorija(request.getParameter("memorija"));
        d.setPodrzaniTipoviKartica(request.getParameter("podrzaniTipoviKartica"));
        d.setGrps(request.getParameter("grps"));
        d.setEdge(request.getParameter("edge"));
        d.setBrzina(request.getParameter("brzina"));
        d.setWlan(request.getParameter("wlan"));
        d.setBluetoth(request.getParameter("bluetoth"));
        d.setNfc(request.getParameter("nfc"));
        d.setUsb(request.getParameter("usb"));
        d.setOperativniSistem(request.getParameter("operativniSistem"));
        d.setVerzija(request.getParameter("verzija"));
        d.setVrstaCipa(request.getParameter("vrstaCipa"));
        d.setGrafickiProcesor(request.getParameter("grafickiProcesor"));
        d.setSenzori(request.getParameter("senzori"));
        d.setFormatiFajlova(request.getParameter("formatiFajlova"));
        
        Telefon t = new Telefon();
        t.setCena(Integer.valueOf(request.getParameter("cena")));
        t.setNaziv(request.getParameter("naziv"));
        t.setProizvodjac(p);
        t.setSlika("slike/"+proizvodjac+"/"+fileName);
        d.setTelefon(t);
        t.setDetalji(d);
        t.setProizvodjac(p);
        
        sesija.save(d);
        sesija.save(t);

        tx.commit();
        sesija.close();
            request.setAttribute("poruka", "Telefon je dodat!");
            request.getRequestDispatcher("administracija.jsp").forward(request, response);
        }catch(Exception e){
            request.setAttribute("poruka", "Telefon nije dodat!");
            request.getRequestDispatcher("administracija.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
