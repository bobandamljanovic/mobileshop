/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servleti;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.HibernateUtil;
import model.Porudzbina;
import model.Telefon;

/**
 *
 * @author FCCZ
 */
@WebServlet(name = "dodajuKorpu", urlPatterns = {"/dodajuKorpu"})
public class dodajuKorpu extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int id = 1;
        boolean kolac = false;
        try {
            
            id = Integer.parseInt(request.getParameter("id"));
            
            Cookie[] cookies = request.getCookies();
        
            for(Cookie c : cookies){
                if(c.getName().equals("korpa")){
                    HibernateUtil.addTelefonToPorudzbina(id, Long.valueOf(c.getValue()));
                    request.setAttribute("idPorudzbina", Long.valueOf(c.getValue()));
                    kolac = true;
                    break;
                }
            }
                if(!kolac){
                    Porudzbina porudzbina = new Porudzbina();
                    Date date = new Date();
                    Long idPorudzbina = date.getTime();
                    porudzbina.setId(idPorudzbina);
                    HibernateUtil.savePorudzbina(porudzbina);
                    HibernateUtil.addTelefonToPorudzbina(id, idPorudzbina);
                    String imeCook = String.valueOf(idPorudzbina);
                    Cookie cook = new Cookie("korpa",imeCook);
                    response.addCookie(cook);
                    request.setAttribute("idPorudzbina", idPorudzbina);
                }
            request.setAttribute("poruka", "Uspešno dodato u korpu !");
            request.setAttribute("telefon", HibernateUtil.getTelefonById(id));
            request.getRequestDispatcher("detalji.jsp").forward(request, response);
           
        }catch(Exception ex){
                request.setAttribute("poruka", "Neuspešno dodavanje u korpu !");
                request.getRequestDispatcher("index.jsp").forward(request, response);     
        }   
            
        
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
