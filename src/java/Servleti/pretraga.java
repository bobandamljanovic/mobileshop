/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servleti;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.HibernateUtil;

/**
 *
 * @author FCCZ
 */
@WebServlet(name = "pretraga", urlPatterns = {"/pretraga"})
public class pretraga extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int proizvodjacID = 0;
        int minCena = 1;
        int maxCena = 100000;
            
        try{
                proizvodjacID = Integer.parseInt(request.getParameter("proizvodjac"));
                
        }catch(Exception ex){
            
            request.setAttribute("poruka", "Pogrešno ukucan ID proizvođača !");
            request.getRequestDispatcher("index.jsp").forward(request, response);
            return;
              
            }
        try{
            
                minCena = Integer.parseInt(request.getParameter("minCena"));
            
        }catch(Exception ex){
            
            request.setAttribute("poruka", "Niste ili ste pogrešno upisali minimalnu cenu !");
            request.getRequestDispatcher("index.jsp").forward(request, response);
            return;
            
            
            }
        try{
                
            maxCena = Integer.parseInt(request.getParameter("maxCena"));
            
        }catch(NumberFormatException ex){
            
            request.setAttribute("poruka", "Niste ili ste pogrešno upisali maksimalnu cenu !");
            request.getRequestDispatcher("index.jsp").forward(request, response);
            return;
             
        }
        
            request.setAttribute("listaTelefona",HibernateUtil.getTelefoneByParametar(proizvodjacID, minCena, maxCena));
            request.getRequestDispatcher("index.jsp").forward(request, response);
            
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}