/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servleti;

import java.util.List;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.HibernateUtil;
import model.Telefon;

/**
 *
 * @author FCCZ
 */
@WebServlet(name = "upisPorudzbine", urlPatterns = {"/upisPorudzbine"})
public class upisPorudzbine extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String ime = "";
        String prezime = "";
        String adresa = "";
        String grad = "";
        String brojTelefona = "";
        long idPorudzbine = 0;
        
        
        try{
            ime = request.getParameter("ime");
            prezime = request.getParameter("prezime");
            adresa = request.getParameter("adresa");
            grad = request.getParameter("grad");
            brojTelefona = request.getParameter("brojTelefona");
            for(Cookie c : request.getCookies()){
                if(c.getName().equals("korpa")){
                    idPorudzbine = Long.valueOf(c.getValue());
                }
            }
            
        }catch(Exception e){
            request.setAttribute("poruka", "Nešto nije kako treba, pokušajte ponovo!");
            request.getRequestDispatcher("korpa.jsp").forward(request, response);
            return;
        }
        
        String potvrda = HibernateUtil.updatePorudzbina(idPorudzbine, ime, prezime, grad, brojTelefona, adresa);
        
        try{
            if(potvrda.equals("ok")){
                Exception e = new Exception();
                throw e;
            }
        }catch(Exception e){
        for(Cookie c : request.getCookies()){
                if(c.getName().equals("korpa")){
                    c.setMaxAge(0);
                    response.addCookie(c);
                }
        }
        
        request.setAttribute("poruka", "Uspešno ste poručili!");
        request.getRequestDispatcher("korpa.jsp").forward(request, response);
        return;
        
        
        }
        for(Cookie c : request.getCookies()){
                if(c.getName().equals("korpa")){
                    c.setMaxAge(0);
                    response.addCookie(c);
                }
        }
        request.setAttribute("poruka", "Nešto nije uredu, pokušajte ponovo!");
        request.getRequestDispatcher("korpa.jsp").forward(request, response);
        
          
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
